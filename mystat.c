#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
struct stat sb;
if (argc != 2) {
fprintf(stderr, "Usage: %s \n", argv[0]);
exit(EXIT_FAILURE);
}
if (stat(argv[1], &sb) == -1) {
perror("stat");
exit(EXIT_FAILURE);
}
printf("文件: %s\n",argv[1]);
printf("I-node: %ld\n", (long) sb.st_ino);
printf("硬连接: %ld\n", (long) sb.st_nlink);
printf("权限: UID=%ld GID=%ld\n",(long) sb.st_uid, (long) sb.st_gid);
printf("IO块: %ld ",(long) sb.st_blksize);
switch (sb.st_mode & S_IFMT) {
case S_IFBLK: printf("块设备\n");
break;
case S_IFCHR: printf("character device\n");
break;
case S_IFDIR: printf("目录\n");
break;
case S_IFIFO: printf("FIFO/管道\n");
break;
case S_IFLNK: printf("符号链接\n");
break;
case S_IFREG: printf("普通文件\n");
break;
case S_IFSOCK: printf("socket\n");
break;
default: printf("未知?\n");
break;
}
printf("大小: %lld bytes\n",(long long) sb.st_size);
printf("块: %lld\n",(long long) sb.st_blocks);
printf("最近访问: %s", ctime(&sb.st_atime));
printf("最近更改: %s", ctime(&sb.st_mtime));
printf("最近改动: %s", ctime(&sb.st_ctime));
exit(EXIT_SUCCESS);
}