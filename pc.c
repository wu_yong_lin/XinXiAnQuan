#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <semaphore.h>

#define NUM 3
int queue[NUM];
sem_t blank_number, product_number;

void *producer ( void * arg )
{
	static int p = 0;

	for ( ;; ) {
		sem_wait( &blank_number );
		queue[p] = rand() % 1000;
		printf("Product %d \n", queue[p]);
		p = (p+1) % NUM;
		sleep ( rand() % 5);
		sem_post( &product_number );
	}
}
void *consumer ( void * arg )
{

	static int c = 0;
	for( ;; ) {
		sem_wait( &product_number );
		printf("Consume %d\n", queue[c]);
		c = (c+1) % NUM;
		sleep( rand() % 5 );
		sem_post( &blank_number );
	}
}

int main(int argc, char *argv[] )
{
	pthread_t pid, cid,cid1,cid2,cid3,cid4,cid5;
    
	sem_init( &blank_number, 0, NUM );
	sem_init( &product_number, 0, 0);
	pthread_create( &pid, NULL, producer, NULL);
	pthread_create( &cid, NULL, consumer, NULL);
	pthread_create( &cid1, NULL, consumer, NULL);
	pthread_create( &cid2, NULL, consumer, NULL);
	pthread_create( &cid3, NULL, consumer, NULL);
	pthread_create( &cid4, NULL, consumer, NULL);
	pthread_create( &cid5, NULL, consumer, NULL);
	pthread_join( pid, NULL );
	pthread_join( cid, NULL );
	pthread_join( cid1, NULL );
	pthread_join( cid2, NULL );
	pthread_join( cid3, NULL );
	pthread_join( cid4, NULL );
	pthread_join( cid5, NULL );
	sem_destroy( &blank_number );
	sem_destroy( &product_number );
	return 0;
}
