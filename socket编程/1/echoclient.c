#include <stdio.h>
#include <netinet/in.h>                                                                   
#include <arpa/inet.h>
#include <netdb.h>    
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>

#define RIO_BUFSIZE 8192

typedef struct{
    int rio_fd;
    int rio_cnt;
    char *rio_bufptr;
    char rio_buf[RIO_BUFSIZE];

}rio_t;

#define MAXLINE 200

int main(int argc,char **argv){

    int clientfd,port;
    char *host,buf[MAXLINE];
    char sbuf[MAXLINE];
    char rbuf[MAXLINE];
    rio_t rio;
    char str1[MAXLINE]="客户端IP:";
    char str2[MAXLINE]="服务器实现者学号:20191322";
    
    char str3[MAXLINE]="echo:";

    if(argc!=3){
    
        fprintf(stderr,"usage:%s <host> <port>\n",argv[0]);
        exit(0);
    }
    host = argv[1];
    port = atoi(argv[2]);

    clientfd = open_clientfd(host,port);
  
    while(1){
        gets(sbuf);
        send(clientfd,sbuf,MAXLINE,0);

        recv(clientfd,rbuf,MAXLINE,0);

        printf("%s",str1);
        puts(host);

        printf("%s",str2);
        putchar('\n');

        printf("%s",str3);

        puts(rbuf);
       
        close(clientfd);
       
        exit(0);
    }

}