<%@ page language="java" import="java.util.*,java.sql.*,java.util.Date,java.text.SimpleDateFormat" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!-- 
功能介绍：添加DAO

 -->
<%@ include file="/files/db/conn.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
</head>
<body>
<%
PreparedStatement psm = null;
try {
	psm = conn.prepareStatement("insert into t_user (username,password,realname,role,sex,departmentid,zhiwei,tel,email,zuzhiid) values(?,?,?,2,?,?,?,?,?,?)");
	psm.setString(1, request.getParameter("username")!=null?request.getParameter("username"):"");
	psm.setString(2, request.getParameter("password")!=null?request.getParameter("password"):"");
	psm.setString(3, request.getParameter("realname")!=null?request.getParameter("realname"):"");
	psm.setString(4, request.getParameter("sex")!=null?request.getParameter("sex"):"");
	psm.setString(5, request.getParameter("departmentid")!=null?request.getParameter("departmentid"):"");
	psm.setString(6, request.getParameter("zhiwei")!=null?request.getParameter("zhiwei"):"");
	psm.setString(7, request.getParameter("tel")!=null?request.getParameter("tel"):"");
	psm.setString(8, request.getParameter("email")!=null?request.getParameter("email"):"");
	psm.setString(9, request.getParameter("zuzhiid")!=null?request.getParameter("zuzhiid"):"");
	psm.executeUpdate();
	
	response.sendRedirect("list.jsp?flag=success");

} catch (Exception e) {
	out.print(e.getMessage());
} finally {
	if (stmt != null) {
		try {
			stmt.close();
		} catch (Exception e) {
	e.printStackTrace();
		}
	}
	if (psm != null) {
		try {
	psm.close();
		} catch (Exception e) {
	e.printStackTrace();
		}
	}
	if (conn != null) {
		try {
	conn.close();
		} catch (Exception e) {
	e.printStackTrace();
		}
	}
}
%>
</body>
</html>
