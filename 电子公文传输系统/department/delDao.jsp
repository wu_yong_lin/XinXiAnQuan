<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!-- 
功能介绍：删除数据处理DAO

 -->
<%@ include file="/files/db/conn.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
</head>
<body>
<%
PreparedStatement psm = null;

try {
	psm = conn.prepareStatement("delete from  t_department  where id = "+request.getParameter("ID"));
	
	psm.executeUpdate();
	response.sendRedirect("list.jsp?flag=success");

} catch (Exception e) {
	out.print(e.getMessage());
} finally {
	if (psm != null) {
		try {
	psm.close();
		} catch (Exception e) {
	e.printStackTrace();
		}
	}
	if (conn != null) {
		try {
	conn.close();
		} catch (Exception e) {
	e.printStackTrace();
		}
	}
}
%>
</body>
</html>
